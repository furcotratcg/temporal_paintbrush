# About
This project is a database of all neopets portaits of all colors, including older versions.

# Installation
```
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
cd temporal_paintbrush
python manage.py migrate
python manage.py runserver
```