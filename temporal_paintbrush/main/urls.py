from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from main import views

urlpatterns = [
    path('', views.api_root),
    path('tags/', views.TagList.as_view(), name='tag-list'),
    path('tags/<int:pk>/', views.TagDetail.as_view(), name='tag-detail'),
    path('portraits/', views.PortraitList.as_view(), name='portrait-list'),
    path('portraits/<int:pk>/', views.PortraitDetail.as_view(), name='portrait-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)