from django.contrib.auth.models import User, Group
from main.models import Tag, Portrait
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups', 'created_tags'],


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']

class TagSerializer(serializers.ModelSerializer):
    tag_created_by = serializers.ReadOnlyField(source='tag_created_by.username')
    class Meta:
        model = Tag
        fields = ['id','name', 'tag_created_by']

class PortraitSerializer(serializers.ModelSerializer):
    portrait_created_by = serializers.ReadOnlyField(source='portrait_created_by.username')
    class Meta:
        model = Portrait
        fields = ['id','image', 'tags', 'portrait_created_by']
    # image = serializers.ImageField()
    # tags = serializers.SlugRelatedField(
    #     many=True,
    #     slug_field=Tag.name,
    #     queryset=Tag.objects.all()
    # )
