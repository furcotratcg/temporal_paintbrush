from django.db import models
from django.contrib.auth.models import User

# Create your models here.

def portraitFile(instance, filename):
    return '/'.join( ['portraits', str(instance.id), filename] )

class Tag(models.Model):
    name = models.CharField(max_length=1000, unique=True)
    created = models.DateTimeField(auto_now_add=True)
    tag_created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="created_tags")
    def __str__(self):
        return self.name

class Portrait(models.Model):
    image = models.ImageField(
        upload_to=portraitFile,
        max_length=254, blank=True, null=True
    )
    created = models.DateTimeField(auto_now_add=True)
    portrait_created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="created_portraits")
    tags = models.ManyToManyField(Tag, related_name="tagname", related_query_name="tagname")
